import React from 'react'
import "./Register.css";

const Register = () => {
    return(
      <article>
        <section className="register">

          <h1 className="register__title">Register</h1>

          <form className="register__form">
            <label className="register__form__label">E-mail:</label>
            <input type="text" className="register__form__input"></input>

            <label className="register__form__label">Wachtwoord</label>
            <input type="password" className="register__form__input"></input>

            <button className="register__form__button blueButton">Inloggen</button>
          </form>

          <section className="registern__registerBtn">
            <h2 className="register__registerBtn__h2">Nog geen account?</h2>
            <button className="register__registerBtn__button blueButton">Registreer</button>
          </section>


        </section>
      </article>

    );
}

export default Register;
