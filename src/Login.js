import React from 'react'
import "./Login.css";

class Login extends React.Component {
  render() {
    return(
      <article>
        <section className="login">

          <h1 className="login__title">Login</h1>

          <form className="login__form">
            <label className="login__form__label">E-mail:</label>
            <input type="text" className="login__form__input"></input>

            <label className="login__form__label">Wachtwoord</label>
            <input type="password" className="login__form__input"></input>

            <button className="login__form__button blueButton">Inloggen</button>
          </form>

          <section className="login__registerBtn">
            <h2 className="login__registerBtn__h2">Nog geen account?</h2>
            <button className="login__registerBtn__button blueButton">Registreer</button>
          </section>


        </section>
      </article>

    );
  }
}

export default Login;
